#ifndef ITOG_H
#define ITOG_H

#include <QWidget>
#include <QTableWidget>
#include <QLineEdit>
#include <QtCharts/QtCharts>

QT_CHARTS_USE_NAMESPACE

class Itog : public QWidget
{
    Q_OBJECT
    
public:
    Itog(QWidget *parent = 0);
    ~Itog();
private:
    QTableWidget *tableXY;
    QTableWidget *hordMethod;
    QLineEdit *wrA, *wrB, *wrC;
    QChartView *m_chartView;
    QStatusBar *statusBar;
    
public slots:
    void onUpdate();
    void onRect(QPointF point);
};

#endif // ITOG_H
