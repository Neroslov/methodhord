#include "itog.h"
#include <QTableWidgetItem>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

Itog::Itog(QWidget *parent)
    : QWidget(parent)
{
    this->resize(770,100);
    tableXY =new QTableWidget();
        tableXY->hide();
        tableXY->setMaximumWidth(220);
        tableXY->setEditTriggers(QAbstractItemView::NoEditTriggers);
        
    hordMethod =new QTableWidget();
        hordMethod->hide();
        hordMethod->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_chartView =new QChartView();
        m_chartView->hide();
        m_chartView->setRenderHint(QPainter::Antialiasing);
    
    wrA =new QLineEdit(); wrA->setPlaceholderText("Введите A");
    wrB =new QLineEdit(); wrB->setPlaceholderText("Введите B");
    wrC =new QLineEdit(); wrC->setPlaceholderText("Введите C");
    
    statusBar =new QStatusBar();
    QPushButton *pR =new QPushButton("ax^3+bx-c");
    connect(pR, SIGNAL(clicked(bool)), this, SLOT(onUpdate()));
    
    QHBoxLayout *lineEdit_lay =new QHBoxLayout;
        lineEdit_lay->addWidget(wrA);
        lineEdit_lay->addWidget(wrB);
        lineEdit_lay->addWidget(wrC);
    
    QHBoxLayout *tableAndChart_lay =new QHBoxLayout;
        tableAndChart_lay->addWidget(tableXY,1);
        tableAndChart_lay->addWidget(m_chartView,2);
    
    QVBoxLayout *this_lay =new QVBoxLayout;
        this_lay->addLayout(lineEdit_lay);
        this_lay->addWidget(pR);
        this_lay->addLayout(tableAndChart_lay);
        this_lay->addWidget(hordMethod);
        this_lay->addWidget(statusBar);
    
    this->setLayout(this_lay);
    
}

Itog::~Itog()
{
    
}


void Itog::onUpdate()
{
    if(!wrA->text().toDouble() || !wrB->text().toDouble() || !wrC->text().toDouble()){ statusBar->showMessage("Неверно введено A|B|C"); return;}
    try{
        tableXY->show();
        
        QVector <double> vectorX;
        QVector <double> vectorY;
        
        double a,b,c;
        a =wrA->text().toDouble();
        b =wrB->text().toDouble();
        c =wrC->text().toDouble();
        
//Расчет ax^3+bx-c и построение таблицы tableXY
        {
            for(double x =1; x <=3.1; x+=0.1){
                vectorX.append(x);
                vectorY.append(a*pow(x,3)+b*x-c);
            }
            tableXY->clear(); tableXY->setRowCount(vectorX.size()); tableXY->setColumnCount(2);
            tableXY->setHorizontalHeaderLabels(QString("X;Y").split(";"));
            tableXY->verticalHeader()->hide();
            
            for(int i =0; i< vectorX.size(); i++){
                tableXY->setItem(i,0, new QTableWidgetItem(QString::number(vectorX.at(i))));
                tableXY->setItem(i,1, new QTableWidgetItem(QString::number(vectorY.at(i))));
            }
        }
//Построение графика
        {
            m_chartView->show();
            QChart *m_chart =new QChart();
            
            m_chart->setAnimationOptions(QChart::SeriesAnimations);
            QSplineSeries *series = new QSplineSeries;
            series->setUseOpenGL(true);
            
            m_chart->legend()->hide();
            m_chart->setTitle(""); //Пиши что хочешь
            
            QValueAxis *axisX = new QValueAxis;
            axisX->setTickCount(7);
            axisX->setTitleText("Ось X");
            m_chart->addAxis(axisX, Qt::AlignBottom);
            
            for(int i =0; i< vectorX.size(); i++){
                *series<<QPointF(vectorX.at(i), vectorY.at(i));
            }
            m_chart->addSeries(series);
    //Установка толщины шрифта
            {
                QPen pen =series->pen();
                pen.setWidth(5);
                series->setPen(pen);
            }
            connect(series, SIGNAL(clicked(QPointF)), this, SLOT(onRect(QPointF)));
            QValueAxis *axisY = new QValueAxis;
            axisY->setTitleText("Ось Y");
            m_chart->addAxis(axisY, Qt::AlignLeft);
            
            series->attachAxis(axisX);
            series->attachAxis(axisY);
            
            m_chartView->setChart(m_chart);
        }
//Расчет методом Хорда и построение таблицы
        {
            hordMethod->clear();
            hordMethod->setColumnCount(7);
            hordMethod->setRowCount(20);
            hordMethod->setMinimumHeight(200);
            hordMethod->setHorizontalHeaderLabels(QString("a;c;b;f(a);f(c);f(b);Точность").split(";"));
            QVector <QVector<double>> vectorHord =QVector <QVector<double>>(20, QVector<double>(7));
            
            vectorHord[0][0] =1;//a
            vectorHord[0][2] =3;//b
            vectorHord[0][3] =a*pow(vectorHord[0][0],3)+b*vectorHord[0][0]-c;//f(a)
            vectorHord[0][5] =a*pow(vectorHord[0][2],3)+b*vectorHord[0][2]-c;//f(b)
            vectorHord[0][1] =vectorHord[0][0]-((vectorHord[0][2]-vectorHord[0][0])*vectorHord[0][3]/(vectorHord[0][5]-vectorHord[0][3]));//c
            vectorHord[0][4] =a*pow(vectorHord[0][1],3)+b*vectorHord[0][1]-c;//f(c)
            vectorHord[0][6] =fabs(vectorHord[0][2]-vectorHord[0][0]);//Точность
            
            for(int i =1; i<20; i++){ //начинаем с 1, тк 0 мы уже заполнили
                    vectorHord[i][0] =vectorHord[i-1][4]*vectorHord[i-1][5] <0 ?vectorHord[i-1][1] :vectorHord[i-1][0];//a
                    vectorHord[i][2] =vectorHord[i-1][4]*vectorHord[i-1][3] <0 ?vectorHord[i-1][1] :vectorHord[i-1][2];//b
                    vectorHord[i][3] =a*pow(vectorHord[i][0],3)+b*vectorHord[i][0]-c;//f(a)
                    vectorHord[i][5] =a*pow(vectorHord[i][2],3)+b*vectorHord[i][2]-c;//f(b)
                    vectorHord[i][1] =vectorHord[i][0]-((vectorHord[i][2]-vectorHord[i][0])*vectorHord[i][3]/(vectorHord[i][5]-vectorHord[i][3]));//c
                    vectorHord[i][4] =a*pow(vectorHord[i][1],3)+b*vectorHord[i][1]-c;//f(c)
                    vectorHord[i][6] =fabs(vectorHord[i][2]-vectorHord[i][0]);//Точность
            }
            hordMethod->show();
            for(int i =0; i<20;i++)
                for(int j =0; j<7; j++){
                    hordMethod->setItem(i,j, new QTableWidgetItem(QString::number(vectorHord[i][j])));
                }

        }
        statusBar->showMessage("Успешно");
        
    }catch(int){
        statusBar->showMessage("Неудачно");    
    }
    
}

void Itog::onRect(QPointF point)
{
    statusBar->showMessage(QString("X :%1       Y :%2").arg(point.x()).arg(point.y()));
}
